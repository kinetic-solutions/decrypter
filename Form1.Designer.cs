﻿
namespace Decrypter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGo = new System.Windows.Forms.Button();
            this.lblApp = new System.Windows.Forms.Label();
            this.lblconn = new System.Windows.Forms.Label();
            this.cboApp = new System.Windows.Forms.ComboBox();
            this.cboSiteVer = new System.Windows.Forms.ComboBox();
            this.lblSiteVer = new System.Windows.Forms.Label();
            this.cboIdent = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.chkStopOnFirst = new System.Windows.Forms.CheckBox();
            this.lbResults = new System.Windows.Forms.ListBox();
            this.btnOpenSelected = new System.Windows.Forms.Button();
            this.btnChooseConfig = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cboDb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAllocationId = new System.Windows.Forms.TextBox();
            this.btnOpenAcceptance = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDb = new System.Windows.Forms.Label();
            this.cboResourceType = new System.Windows.Forms.ComboBox();
            this.lblResourceType = new System.Windows.Forms.Label();
            this.lblIndentifierContains = new System.Windows.Forms.Label();
            this.txtIdentifierContains = new System.Windows.Forms.TextBox();
            this.txtWAFind = new System.Windows.Forms.TextBox();
            this.txtWAReplace = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFindReplace = new System.Windows.Forms.Button();
            this.panelFixWebAgreements = new System.Windows.Forms.Panel();
            this.chkFRDebug = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFRMinDate = new System.Windows.Forms.TextBox();
            this.chkStopOnFind = new System.Windows.Forms.CheckBox();
            this.chkOpenWAInNotepad = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtCorrespondenceId = new System.Windows.Forms.TextBox();
            this.lblCorrespondenceId = new System.Windows.Forms.Label();
            this.rbWAtb2 = new System.Windows.Forms.RadioButton();
            this.rbWAtb1 = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnEditBadHtml = new System.Windows.Forms.Button();
            this.btnViewBadHTML = new System.Windows.Forms.Button();
            this.lbHTMLCheckResults = new System.Windows.Forms.ListBox();
            this.btnCheckLocationContent = new System.Windows.Forms.Button();
            this.panelFixWebAgreements.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGo
            // 
            this.btnGo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGo.Location = new System.Drawing.Point(632, 236);
            this.btnGo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(91, 30);
            this.btnGo.TabIndex = 0;
            this.btnGo.Text = "Open";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // lblApp
            // 
            this.lblApp.AutoSize = true;
            this.lblApp.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApp.Location = new System.Drawing.Point(13, 16);
            this.lblApp.Name = "lblApp";
            this.lblApp.Size = new System.Drawing.Size(97, 21);
            this.lblApp.TabIndex = 2;
            this.lblApp.Text = "Application";
            // 
            // lblconn
            // 
            this.lblconn.AutoSize = true;
            this.lblconn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblconn.Location = new System.Drawing.Point(0, 654);
            this.lblconn.Name = "lblconn";
            this.lblconn.Size = new System.Drawing.Size(10, 16);
            this.lblconn.TabIndex = 3;
            this.lblconn.Text = " ";
            // 
            // cboApp
            // 
            this.cboApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboApp.FormattingEnabled = true;
            this.cboApp.Location = new System.Drawing.Point(19, 40);
            this.cboApp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboApp.Name = "cboApp";
            this.cboApp.Size = new System.Drawing.Size(399, 30);
            this.cboApp.TabIndex = 4;
            this.cboApp.SelectedValueChanged += new System.EventHandler(this.cboApp_SelectedValueChanged);
            // 
            // cboSiteVer
            // 
            this.cboSiteVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboSiteVer.FormattingEnabled = true;
            this.cboSiteVer.Location = new System.Drawing.Point(21, 171);
            this.cboSiteVer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboSiteVer.Name = "cboSiteVer";
            this.cboSiteVer.Size = new System.Drawing.Size(399, 30);
            this.cboSiteVer.TabIndex = 6;
            this.cboSiteVer.SelectedValueChanged += new System.EventHandler(this.cboSiteVer_SelectedValueChanged);
            // 
            // lblSiteVer
            // 
            this.lblSiteVer.AutoSize = true;
            this.lblSiteVer.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSiteVer.Location = new System.Drawing.Point(15, 146);
            this.lblSiteVer.Name = "lblSiteVer";
            this.lblSiteVer.Size = new System.Drawing.Size(105, 21);
            this.lblSiteVer.TabIndex = 5;
            this.lblSiteVer.Text = "Site Version";
            // 
            // cboIdent
            // 
            this.cboIdent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboIdent.FormattingEnabled = true;
            this.cboIdent.Location = new System.Drawing.Point(21, 236);
            this.cboIdent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboIdent.Name = "cboIdent";
            this.cboIdent.Size = new System.Drawing.Size(605, 30);
            this.cboIdent.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "Identifier";
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(21, 317);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(605, 28);
            this.txtSearch.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 292);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 21);
            this.label4.TabIndex = 10;
            this.label4.Text = "Text Contains...";
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(631, 384);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(92, 50);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 358);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 21);
            this.label5.TabIndex = 13;
            this.label5.Text = "Search Results";
            // 
            // chkStopOnFirst
            // 
            this.chkStopOnFirst.AutoSize = true;
            this.chkStopOnFirst.Location = new System.Drawing.Point(387, 354);
            this.chkStopOnFirst.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkStopOnFirst.Name = "chkStopOnFirst";
            this.chkStopOnFirst.Size = new System.Drawing.Size(197, 20);
            this.chkStopOnFirst.TabIndex = 14;
            this.chkStopOnFirst.Text = "Stop on first match and open";
            this.chkStopOnFirst.UseVisualStyleBackColor = true;
            // 
            // lbResults
            // 
            this.lbResults.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResults.FormattingEnabled = true;
            this.lbResults.ItemHeight = 17;
            this.lbResults.Location = new System.Drawing.Point(21, 384);
            this.lbResults.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbResults.Name = "lbResults";
            this.lbResults.Size = new System.Drawing.Size(605, 106);
            this.lbResults.TabIndex = 16;
            // 
            // btnOpenSelected
            // 
            this.btnOpenSelected.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenSelected.Location = new System.Drawing.Point(632, 443);
            this.btnOpenSelected.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOpenSelected.Name = "btnOpenSelected";
            this.btnOpenSelected.Size = new System.Drawing.Size(91, 30);
            this.btnOpenSelected.TabIndex = 17;
            this.btnOpenSelected.Text = "Open";
            this.btnOpenSelected.UseVisualStyleBackColor = true;
            this.btnOpenSelected.Click += new System.EventHandler(this.btnOpenSelected_Click);
            // 
            // btnChooseConfig
            // 
            this.btnChooseConfig.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseConfig.Location = new System.Drawing.Point(20, 11);
            this.btnChooseConfig.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnChooseConfig.Name = "btnChooseConfig";
            this.btnChooseConfig.Size = new System.Drawing.Size(211, 38);
            this.btnChooseConfig.TabIndex = 19;
            this.btnChooseConfig.Text = "Choose web.config";
            this.btnChooseConfig.UseVisualStyleBackColor = true;
            this.btnChooseConfig.Click += new System.EventHandler(this.btnChooseConfig_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(257, 11);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(194, 38);
            this.button1.TabIndex = 20;
            this.button1.Text = "Choose database";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboDb
            // 
            this.cboDb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboDb.FormattingEnabled = true;
            this.cboDb.Location = new System.Drawing.Point(457, 16);
            this.cboDb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboDb.Name = "cboDb";
            this.cboDb.Size = new System.Drawing.Size(295, 26);
            this.cboDb.TabIndex = 21;
            this.cboDb.Visible = false;
            this.cboDb.SelectedIndexChanged += new System.EventHandler(this.cboDb_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(231, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 21);
            this.label6.TabIndex = 22;
            this.label6.Text = "or";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtAllocationId
            // 
            this.txtAllocationId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAllocationId.Location = new System.Drawing.Point(27, 115);
            this.txtAllocationId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAllocationId.Name = "txtAllocationId";
            this.txtAllocationId.Size = new System.Drawing.Size(84, 28);
            this.txtAllocationId.TabIndex = 23;
            // 
            // btnOpenAcceptance
            // 
            this.btnOpenAcceptance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenAcceptance.Location = new System.Drawing.Point(218, 115);
            this.btnOpenAcceptance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOpenAcceptance.Name = "btnOpenAcceptance";
            this.btnOpenAcceptance.Size = new System.Drawing.Size(122, 30);
            this.btnOpenAcceptance.TabIndex = 24;
            this.btnOpenAcceptance.Text = "Open";
            this.btnOpenAcceptance.UseVisualStyleBackColor = true;
            this.btnOpenAcceptance.Click += new System.EventHandler(this.btnOpenAcceptance_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(24, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 25;
            this.label7.Text = "AllocationId";
            // 
            // lblDb
            // 
            this.lblDb.AutoSize = true;
            this.lblDb.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDb.Location = new System.Drawing.Point(21, 51);
            this.lblDb.Name = "lblDb";
            this.lblDb.Size = new System.Drawing.Size(24, 17);
            this.lblDb.TabIndex = 28;
            this.lblDb.Text = "db";
            this.lblDb.Visible = false;
            // 
            // cboResourceType
            // 
            this.cboResourceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboResourceType.FormattingEnabled = true;
            this.cboResourceType.Items.AddRange(new object[] {
            "CONFIG",
            "E-Mail",
            "KCF.Environment.WebEditor",
            "Pages",
            "Sql",
            "User"});
            this.cboResourceType.Location = new System.Drawing.Point(19, 105);
            this.cboResourceType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cboResourceType.Name = "cboResourceType";
            this.cboResourceType.Size = new System.Drawing.Size(399, 30);
            this.cboResourceType.TabIndex = 29;
            this.cboResourceType.SelectedValueChanged += new System.EventHandler(this.cboResourceType_SelectedValueChanged);
            // 
            // lblResourceType
            // 
            this.lblResourceType.AutoSize = true;
            this.lblResourceType.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResourceType.Location = new System.Drawing.Point(15, 80);
            this.lblResourceType.Name = "lblResourceType";
            this.lblResourceType.Size = new System.Drawing.Size(50, 21);
            this.lblResourceType.TabIndex = 30;
            this.lblResourceType.Text = "Type";
            // 
            // lblIndentifierContains
            // 
            this.lblIndentifierContains.AutoSize = true;
            this.lblIndentifierContains.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIndentifierContains.Location = new System.Drawing.Point(15, 16);
            this.lblIndentifierContains.Name = "lblIndentifierContains";
            this.lblIndentifierContains.Size = new System.Drawing.Size(169, 21);
            this.lblIndentifierContains.TabIndex = 32;
            this.lblIndentifierContains.Text = "Identifier Contains...";
            this.lblIndentifierContains.Visible = false;
            // 
            // txtIdentifierContains
            // 
            this.txtIdentifierContains.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentifierContains.Location = new System.Drawing.Point(19, 42);
            this.txtIdentifierContains.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIdentifierContains.Name = "txtIdentifierContains";
            this.txtIdentifierContains.Size = new System.Drawing.Size(401, 28);
            this.txtIdentifierContains.TabIndex = 31;
            this.txtIdentifierContains.Visible = false;
            this.txtIdentifierContains.Validated += new System.EventHandler(this.txtIdentifierContains_Validated);
            // 
            // txtWAFind
            // 
            this.txtWAFind.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWAFind.Location = new System.Drawing.Point(13, 41);
            this.txtWAFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtWAFind.Name = "txtWAFind";
            this.txtWAFind.Size = new System.Drawing.Size(630, 28);
            this.txtWAFind.TabIndex = 33;
            // 
            // txtWAReplace
            // 
            this.txtWAReplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWAReplace.Location = new System.Drawing.Point(13, 76);
            this.txtWAReplace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtWAReplace.Name = "txtWAReplace";
            this.txtWAReplace.Size = new System.Drawing.Size(630, 28);
            this.txtWAReplace.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 21);
            this.label1.TabIndex = 35;
            this.label1.Text = "Find/Replace";
            // 
            // btnFindReplace
            // 
            this.btnFindReplace.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindReplace.Location = new System.Drawing.Point(554, 120);
            this.btnFindReplace.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFindReplace.Name = "btnFindReplace";
            this.btnFindReplace.Size = new System.Drawing.Size(89, 30);
            this.btnFindReplace.TabIndex = 36;
            this.btnFindReplace.Text = "Update";
            this.btnFindReplace.UseVisualStyleBackColor = true;
            this.btnFindReplace.Click += new System.EventHandler(this.btnFindReplace_Click);
            // 
            // panelFixWebAgreements
            // 
            this.panelFixWebAgreements.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelFixWebAgreements.Controls.Add(this.chkFRDebug);
            this.panelFixWebAgreements.Controls.Add(this.label2);
            this.panelFixWebAgreements.Controls.Add(this.txtFRMinDate);
            this.panelFixWebAgreements.Controls.Add(this.chkStopOnFind);
            this.panelFixWebAgreements.Controls.Add(this.btnFindReplace);
            this.panelFixWebAgreements.Controls.Add(this.txtWAFind);
            this.panelFixWebAgreements.Controls.Add(this.txtWAReplace);
            this.panelFixWebAgreements.Controls.Add(this.label1);
            this.panelFixWebAgreements.Location = new System.Drawing.Point(28, 187);
            this.panelFixWebAgreements.Name = "panelFixWebAgreements";
            this.panelFixWebAgreements.Size = new System.Drawing.Size(664, 203);
            this.panelFixWebAgreements.TabIndex = 37;
            // 
            // chkFRDebug
            // 
            this.chkFRDebug.AutoSize = true;
            this.chkFRDebug.Checked = true;
            this.chkFRDebug.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFRDebug.Location = new System.Drawing.Point(535, 171);
            this.chkFRDebug.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkFRDebug.Name = "chkFRDebug";
            this.chkFRDebug.Size = new System.Drawing.Size(108, 20);
            this.chkFRDebug.TabIndex = 41;
            this.chkFRDebug.Text = "Debug mode";
            this.chkFRDebug.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 16);
            this.label2.TabIndex = 41;
            this.label2.Text = "Only process records created after this date:";
            // 
            // txtFRMinDate
            // 
            this.txtFRMinDate.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFRMinDate.Location = new System.Drawing.Point(305, 166);
            this.txtFRMinDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFRMinDate.Name = "txtFRMinDate";
            this.txtFRMinDate.Size = new System.Drawing.Size(142, 27);
            this.txtFRMinDate.TabIndex = 40;
            // 
            // chkStopOnFind
            // 
            this.chkStopOnFind.AutoSize = true;
            this.chkStopOnFind.Location = new System.Drawing.Point(13, 120);
            this.chkStopOnFind.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkStopOnFind.Name = "chkStopOnFind";
            this.chkStopOnFind.Size = new System.Drawing.Size(375, 20);
            this.chkStopOnFind.TabIndex = 39;
            this.chkStopOnFind.Text = "Stop when first record not containing the Find string is found";
            this.chkStopOnFind.UseVisualStyleBackColor = true;
            // 
            // chkOpenWAInNotepad
            // 
            this.chkOpenWAInNotepad.AutoSize = true;
            this.chkOpenWAInNotepad.Location = new System.Drawing.Point(218, 149);
            this.chkOpenWAInNotepad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkOpenWAInNotepad.Name = "chkOpenWAInNotepad";
            this.chkOpenWAInNotepad.Size = new System.Drawing.Size(123, 20);
            this.chkOpenWAInNotepad.TabIndex = 38;
            this.chkOpenWAInNotepad.Text = "Open as text file";
            this.chkOpenWAInNotepad.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(24, 83);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(831, 569);
            this.tabControl1.TabIndex = 39;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblIndentifierContains);
            this.tabPage1.Controls.Add(this.btnGo);
            this.tabPage1.Controls.Add(this.lblApp);
            this.tabPage1.Controls.Add(this.cboApp);
            this.tabPage1.Controls.Add(this.txtIdentifierContains);
            this.tabPage1.Controls.Add(this.lblSiteVer);
            this.tabPage1.Controls.Add(this.lblResourceType);
            this.tabPage1.Controls.Add(this.cboSiteVer);
            this.tabPage1.Controls.Add(this.cboResourceType);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.cboIdent);
            this.tabPage1.Controls.Add(this.txtSearch);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btnSearch);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.chkStopOnFirst);
            this.tabPage1.Controls.Add(this.lbResults);
            this.tabPage1.Controls.Add(this.btnOpenSelected);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(823, 540);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "WebResources";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtCorrespondenceId);
            this.tabPage2.Controls.Add(this.lblCorrespondenceId);
            this.tabPage2.Controls.Add(this.rbWAtb2);
            this.tabPage2.Controls.Add(this.rbWAtb1);
            this.tabPage2.Controls.Add(this.btnOpenAcceptance);
            this.tabPage2.Controls.Add(this.chkOpenWAInNotepad);
            this.tabPage2.Controls.Add(this.txtAllocationId);
            this.tabPage2.Controls.Add(this.panelFixWebAgreements);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(823, 540);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Web Agreement";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtCorrespondenceId
            // 
            this.txtCorrespondenceId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorrespondenceId.Location = new System.Drawing.Point(123, 115);
            this.txtCorrespondenceId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCorrespondenceId.Name = "txtCorrespondenceId";
            this.txtCorrespondenceId.Size = new System.Drawing.Size(84, 28);
            this.txtCorrespondenceId.TabIndex = 41;
            // 
            // lblCorrespondenceId
            // 
            this.lblCorrespondenceId.AutoSize = true;
            this.lblCorrespondenceId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorrespondenceId.Location = new System.Drawing.Point(120, 92);
            this.lblCorrespondenceId.Name = "lblCorrespondenceId";
            this.lblCorrespondenceId.Size = new System.Drawing.Size(128, 17);
            this.lblCorrespondenceId.TabIndex = 42;
            this.lblCorrespondenceId.Text = "CorrespondenceId";
            // 
            // rbWAtb2
            // 
            this.rbWAtb2.AutoSize = true;
            this.rbWAtb2.Location = new System.Drawing.Point(26, 45);
            this.rbWAtb2.Name = "rbWAtb2";
            this.rbWAtb2.Size = new System.Drawing.Size(161, 20);
            this.rbWAtb2.TabIndex = 40;
            this.rbWAtb2.Text = "WebStudentAllocation";
            this.rbWAtb2.UseVisualStyleBackColor = true;
            this.rbWAtb2.CheckedChanged += new System.EventHandler(this.rbWAtb2_CheckedChanged);
            // 
            // rbWAtb1
            // 
            this.rbWAtb1.AutoSize = true;
            this.rbWAtb1.Checked = true;
            this.rbWAtb1.Location = new System.Drawing.Point(26, 19);
            this.rbWAtb1.Name = "rbWAtb1";
            this.rbWAtb1.Size = new System.Drawing.Size(199, 20);
            this.rbWAtb1.TabIndex = 39;
            this.rbWAtb1.TabStop = true;
            this.rbWAtb1.Text = "ST2StudentCorrespondence";
            this.rbWAtb1.UseVisualStyleBackColor = true;
            this.rbWAtb1.CheckedChanged += new System.EventHandler(this.rbWAtb1_CheckedChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnEditBadHtml);
            this.tabPage3.Controls.Add(this.btnViewBadHTML);
            this.tabPage3.Controls.Add(this.lbHTMLCheckResults);
            this.tabPage3.Controls.Add(this.btnCheckLocationContent);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(823, 540);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Location Content";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnEditBadHtml
            // 
            this.btnEditBadHtml.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditBadHtml.Location = new System.Drawing.Point(464, 305);
            this.btnEditBadHtml.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditBadHtml.Name = "btnEditBadHtml";
            this.btnEditBadHtml.Size = new System.Drawing.Size(118, 38);
            this.btnEditBadHtml.TabIndex = 23;
            this.btnEditBadHtml.Text = "Edit";
            this.btnEditBadHtml.UseVisualStyleBackColor = true;
            this.btnEditBadHtml.Click += new System.EventHandler(this.btnEditBadHtml_Click);
            // 
            // btnViewBadHTML
            // 
            this.btnViewBadHTML.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewBadHTML.Location = new System.Drawing.Point(588, 305);
            this.btnViewBadHTML.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnViewBadHTML.Name = "btnViewBadHTML";
            this.btnViewBadHTML.Size = new System.Drawing.Size(210, 38);
            this.btnViewBadHTML.TabIndex = 22;
            this.btnViewBadHTML.Text = "View in Notepad++";
            this.btnViewBadHTML.UseVisualStyleBackColor = true;
            this.btnViewBadHTML.Click += new System.EventHandler(this.btnViewBadHTML_Click);
            // 
            // lbHTMLCheckResults
            // 
            this.lbHTMLCheckResults.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHTMLCheckResults.FormattingEnabled = true;
            this.lbHTMLCheckResults.ItemHeight = 17;
            this.lbHTMLCheckResults.Location = new System.Drawing.Point(18, 59);
            this.lbHTMLCheckResults.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbHTMLCheckResults.Name = "lbHTMLCheckResults";
            this.lbHTMLCheckResults.Size = new System.Drawing.Size(780, 242);
            this.lbHTMLCheckResults.TabIndex = 21;
            // 
            // btnCheckLocationContent
            // 
            this.btnCheckLocationContent.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckLocationContent.Location = new System.Drawing.Point(18, 17);
            this.btnCheckLocationContent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCheckLocationContent.Name = "btnCheckLocationContent";
            this.btnCheckLocationContent.Size = new System.Drawing.Size(141, 38);
            this.btnCheckLocationContent.TabIndex = 20;
            this.btnCheckLocationContent.Text = "Check All";
            this.btnCheckLocationContent.UseVisualStyleBackColor = true;
            this.btnCheckLocationContent.Click += new System.EventHandler(this.btnCheckLocationContent_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 670);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblDb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboDb);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnChooseConfig);
            this.Controls.Add(this.lblconn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Decrypter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelFixWebAgreements.ResumeLayout(false);
            this.panelFixWebAgreements.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Label lblApp;
        private System.Windows.Forms.Label lblconn;
        private System.Windows.Forms.ComboBox cboApp;
        private System.Windows.Forms.ComboBox cboSiteVer;
        private System.Windows.Forms.Label lblSiteVer;
        private System.Windows.Forms.ComboBox cboIdent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkStopOnFirst;
        private System.Windows.Forms.ListBox lbResults;
        private System.Windows.Forms.Button btnOpenSelected;
        private System.Windows.Forms.Button btnChooseConfig;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cboDb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAllocationId;
        private System.Windows.Forms.Button btnOpenAcceptance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDb;
        private System.Windows.Forms.ComboBox cboResourceType;
        private System.Windows.Forms.Label lblResourceType;
        private System.Windows.Forms.Label lblIndentifierContains;
        private System.Windows.Forms.TextBox txtIdentifierContains;
        private System.Windows.Forms.TextBox txtWAFind;
        private System.Windows.Forms.TextBox txtWAReplace;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFindReplace;
        private System.Windows.Forms.Panel panelFixWebAgreements;
        private System.Windows.Forms.CheckBox chkOpenWAInNotepad;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RadioButton rbWAtb2;
        private System.Windows.Forms.RadioButton rbWAtb1;
        private System.Windows.Forms.CheckBox chkStopOnFind;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFRMinDate;
        private System.Windows.Forms.CheckBox chkFRDebug;
        private System.Windows.Forms.TextBox txtCorrespondenceId;
        private System.Windows.Forms.Label lblCorrespondenceId;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnCheckLocationContent;
        private System.Windows.Forms.ListBox lbHTMLCheckResults;
        private System.Windows.Forms.Button btnViewBadHTML;
        private System.Windows.Forms.Button btnEditBadHtml;
    }
}

