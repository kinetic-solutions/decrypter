﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace Decrypter
{
    public partial class dlgEdit : Form
    {
        private string _connectionString;
        private string _typename;
        private int _locationContentId;
        public dlgEdit(int locationContentId, string typename, string connectionString, string html)
        {
            InitializeComponent();
            _locationContentId = locationContentId;
            _typename = typename;
            _connectionString = connectionString;
            textBox1.Text = html;
            textBox1.SelectionStart = 0;
            textBox1.SelectionLength = 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(textBox1.Text);
                if (doc.ParseErrors.Any())
                {
                    MessageBox.Show(doc.ParseErrors.ElementAt(0).Reason + " on line " + doc.ParseErrors.ElementAt(0).Line, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Update();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("btnUpdate_Click errored. Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Update()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string fieldname = _typename.Replace(" ", "");
                connection.Open();
                using (SqlCommand command = new SqlCommand(string.Format("UPDATE LocationContent SET {0} = '{1}' WHERE LocationContentId = {2}", fieldname, DecoderEncoder.Base64Encode(textBox1.Text), _locationContentId), connection))
                {
                    try
                    {
                        command.CommandTimeout = 300;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Updated successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Update errored. Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
