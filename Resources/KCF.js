﻿function KCF() {
    KCF.dataControlsDone = false;
    KCF.hidingOnNoContentDone = false;
    KCF.overrideValuesDone = false;
    KCF.addCSSClassDone = false;
    KCF.removeCSSClassDone = false;
    KCF.overrideCheckboxLabelDone = false;
    KCF.overrideButtonLabels = false;
    KCF.placeholdersDone = false;
    KCF.titlesDone = false;
    KCF.OnComplete;
}

function KCFGetDATAKX(fnOnData, sql, filter, filter1, filter2, filter3, filter4, filter5, filter6, filter7, filter8, filter9) {
    var requestString = 'data.kx?sql=' + sql + determineFilter('filter', filter) + determineFilter('filter1', filter1) + determineFilter('filter2', filter2) + determineFilter('filter3', filter3) + determineFilter('filter4', filter4) + determineFilter('filter5', filter5) + determineFilter('filter6', filter6) + determineFilter('filter7', filter7) + determineFilter('filter8', filter8) + determineFilter('filter9', filter9);
    $.getJSON(requestString, fnOnData);
}

function KCFGetRESOURCEKX(fnOnData, resource) {
    var requestString = 'content.kx?resourceid=' + resource;
    $.ajax({
        url: requestString,
        type: 'GET'
    }).done(fnOnData);
}

function determineFilter(label, filter) {
    if (typeof (filter) != "undefined") {
        return '&' + label + '=' + filter;
    }

    return '';
}

function KCFaddSelectOption(target, text, value, disabled, attributeName, attributeValue) {
    var item;
    if (disabled === false) {
        item = $(target).append($("<option></option>").text(text).val(value).attr('disabled', 'disabled'));
    } else {
        item = $(target).append($("<option></option>").text(text).val(value));
    }

    if (typeof (attributeName) != "undefined") {
        $("option[value='" + value + "']", $(item)).attr(attributeName, attributeValue);
    }
}

function KCFclearSelectOption(target, boolIncludeSelect, selectText) {
    $(target).find('option').remove().end();
    if (boolIncludeSelect === true) {
        KCFaddSelectOption(target, selectText);
    }
}

function locateKCFControls() {
    if (!KCF.dataControlsDone) {
        $('[data-control-target-copy]').each(function () {
            $target = $(this);
            $value = $target.attr('data-control-target-copy');
            $source = $('[data-control="' + $value + '"]');
            $($source).children().each(function () {
                $child = $(this);
                var element = $child.clone();
                $target.append(element);
            });
        });
        $('[data-control-target]').each(function () {
            $target = $(this);
            $value = $target.attr('data-control-target');
            $source = $('[data-control="' + $value + '"]');
            $($source).children().each(function () {
                $child = $(this);
                var element = $child.detach();
                $target.append(element);
            });
        });
        $('[data-control-target-table]').each(function () {
            $target = $(this);
            $value = $target.attr('data-control-target-table');
            $source = $('[data-control="' + $value + '"]');
            $('tr', $($source)).each(function () {
                $child = $(this);
                var element = $child.detach();
                $target.append(element);
            });
        });
        $('[data-control-local]').each(function () {
            $source = $(this);
            $container = getLocalControlContainer($(this));
            if (typeof ($container) !== 'undefined') {
                $value = $source.attr('data-control-local');
                $target = $('[data-control-target-local="' + $value + '"]', $container);
                $($source)
                    .children()
                    .each(function () {
                        $child = $(this);
                        var element = $child.detach();
                        $target.append(element);
                    });
            }
        });

        KCF.dataControlsDone = true;
    }
}

function getLocalControlContainer(root) {
    if (typeof ($(root).parent()) !== 'undefined') {
        if (typeof ($(root).parent().attr('control-container')) !== 'undefined') {
            return $(root).parent();
        }
        else {
            return getLocalControlContainer($(root).parent());
        }
    }

    return;
}

function overrideObjectValues() {
    if (!KCF.overrideValuesDone) {
        $('[override-object-value]').each(function () {
            var value = $(this).attr('override-object-value');
            $("[value]", $(this)).each(function () {
                $(this).val(value);
            });
        });
        KCF.overrideValuesDone = true;
    }
}

function overrideButtonLabels() {
    if (!KCF.overrideButtonLabels) {
        $('[override-button-label]').each(function () {
            var value = $(this).attr('override-button-label');
            $("[value]", $(this)).each(function () {
                $(this).val(value);
            });
        });
        KCF.overrideButtonLabels = true;
    }
}

function addCSSClass() {
    if (!KCF.addCSSClassDone) {
        $('[add-css-class]').each(function () {
            var value = $(this).attr('add-css-class');
            $(this).children().each(function () {
                $(this).addClass(value);
            });
        });

        KCF.addCSSClassDone = true;
    }
}

function addPlaceholders() {
    if (!KCF.placeholdersDone) {
        $('[add-placeholder]').each(function () {
            var value = $(this).attr('add-placeholder');
            $(this).children().each(function () {
                $(this).attr('placeholder', value);
            });
        });

        $('select[placeholder]').each(function () {
            var $this = $(this);
            $('option[value=""]', $($this)).html($($this).attr('placeholder'));
            if ($('option[value="0"]', $($this)).html() === '') {
                $('option[value="0"]', $($this)).html($($this).attr('placeholder'));
            }
        });

        KCF.placeholdersDone = true;
    }
}

function addTitles() {
    if (!KCF.titlesDone) {
        $('[add-title]').each(function () {
            var value = $(this).attr('add-title');
            $(this).children().each(function () {
                $(this).attr('title', value);
            });
        });

        KCF.titlesDone = true;
    }
}

function removeCSSClass() {
    if (!KCF.removeCSSClassDone) {
        $('[remove-css-class]').each(function () {
            var value = $(this).attr('remove-css-class');
            $(this).children().each(function () {
                $(this).removeClass(value);
            });
        });

        KCF.removeCSSClassDone = true;
    }
}

function overrideCheckboxLabel() {
    if (!KCF.overrideCheckboxLabelDone) {
        $('[override-checkbox-label]').each(function () {
            var value = $(this).attr('override-checkbox-label');
            var id = $('input[type=checkbox]', $(this)).attr('id');
            $('label[for=' + id + ']').text(value);
            var id = $('input[type=radio]', $(this)).attr('id');
            $('label[for=' + id + ']').text(value);
        });
        KCF.overrideCheckboxLabelDone = true;
    }
}

function dataDisplayControlHideFields() {
    $('[data-displaycontrol]').each(function () {
        var value = $(this).attr('data-displaycontrol');
        if (!(value !== '')) {
            $(this).detach();
        }
    });
    $('[data-displaycontrolbool]').each(function () {
        var value = $(this).attr('data-displaycontrolbool');
        var eqvalues = value.split("===");
        if (eqvalues.length > 1 && typeof (eqvalues[0]) !== 'undefined' && typeof (eqvalues[1]) !== 'undefined') {
            if (!(eqvalues[0].toLowerCase() === eqvalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
        var neqvalues = value.split("!==");
        if (neqvalues.length > 1 && typeof (neqvalues[0]) !== 'undefined' && typeof (neqvalues[1]) !== 'undefined') {
            if (!(neqvalues[0].toLowerCase() !== neqvalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
        var gtvalues = value.split(">>>");
        if (gtvalues.length > 1 && typeof (gtvalues[0]) !== 'undefined' && typeof (gtvalues[1]) !== 'undefined') {
            if (!(gtvalues[0].toLowerCase() > gtvalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
        var ltvalues = value.split("<<<");
        if (ltvalues.length > 1 && typeof (ltvalues[0]) !== 'undefined' && typeof (ltvalues[1]) !== 'undefined') {
            if (!(ltvalues[0].toLowerCase() < ltvalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
        var gtevalues = value.split(">==");
        if (gtevalues.length > 1 && typeof (gtevalues[0]) !== 'undefined' && typeof (gtevalues[1]) !== 'undefined') {
            if (!(gtevalues[0].toLowerCase() >= gtevalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
        var ltevalues = value.split("<==");
        if (ltevalues.length > 1 && typeof (ltevalues[0]) !== 'undefined' && typeof (ltevalues[1]) !== 'undefined') {
            if (!(ltevalues[0].toLowerCase() <= ltevalues[1].toLowerCase())) {
                $(this).detach();
            }
        }
    });
}

function jScriptCheck(elementToShow, elementToHide) {
    elementToHide.toggle(false);
    elementToShow.toggle(true);
}


function ClearValues() {
    $('.KCFClearValue').each(function () {
        $(this).val('');
    });
}

$(document).ready(function () {
    locateKCFControls();
    overrideObjectValues();
    overrideButtonLabels();
    addCSSClass();
    removeCSSClass();
    addPlaceholders();
    addTitles();
    overrideCheckboxLabel();
    dataDisplayControlHideFields();
    HideLoadingGraphic();
    BindButtonsForLoading();
    ClearValues();
    DoToolTips();

    if (typeof (KCF.OnComplete) !== 'undefined') {
        KCF.OnComplete();
    }
});

var urlParams;
(window.onpopstate = function () {
    var match,
        pl = /\+/g, // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
})();

function selectDropdownByText(dropdown, textToFind, fnOnChange) {
    if (textToFind !== '') {
        $("option", dropdown).each(function () {
            if ($(this).text().toLowerCase() === textToFind.toLowerCase() && typeof ($(this).attr('disabled')) === 'undefined') {
                $(this).attr('selected', 'selected');
                if (typeof (fnOnChange) != "undefined") {
                    fnOnChange();
                }
            }
        });
    }
}

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function ShowLoadingGraphic(control) {
    var hRef = $(control).attr('href');
    if (typeof (hRef) != "undefined" && hRef.substr(0, 4) == "java" && typeof (Page_ClientValidate) != "undefined" && !$(control).hasClass('Previous') && !$(control).hasClass('NoValidate')) {
        Page_ClientValidate();
    }
    if (typeof (Page_IsValid) == "undefined" || Page_IsValid) {
        var position = $('body').position();
        var left = position.left;
        var top = position.top;
        var width = $(document).width();
        var height = $(document).height();
        //var left = $('#Wrapper').offset().left;
        //var top = $('#Wrapper').offset().top;
        $("#Loading").fadeTo('fast', 0.90)
            .width(width)
            .height(height)
            .css("left", left)
            .css("top", top);
    }
}

function HideLoadingGraphic() {
    $("#Loading").hide();
}

function BindButtonsForLoading() {
    $('input[type=button]:not(.NoLoading)').bind('click', function () {
        ShowLoadingGraphic(this);
    });
    $('input[type=submit]:not(.NoLoading)').bind('click', function () {
        ShowLoadingGraphic(this);
    });
    $('a.jconfirm_success').bind('click', function () {
        ShowLoadingGraphic(this);
    });
}


function DoToolTips() {
    // Notice the use of the each() method to acquire access to each elements attributes
    $('[tooltip]').each(function () {
        $(this).qtip({
            content: $(this).attr('tooltip'),
            position: {
                at: "right center",
                my: "left center"
            },
            style: {
                tip: true,
                classes: "ui-tooltip-cream"
            }
        });
    });
    $('[tooltiptop]').each(function () {
        $(this).qtip({
            content: $(this).attr('tooltiptop'),
            position: {
                at: "top center",
                my: "bottom center"
            },
            style: {
                tip: true,
                classes: "ui-tooltip-cream"
            }
        });
    });
}

jQuery.fn.inputHints = function () {
    $(this).each(function (i) {
        var $this = $(this);
        var $type = $(this).attr('type');
        if ($(this).val() == '') {
            $(this).val($(this).attr('title'));
        }
        $(this).closest("form").submit(function () {
            if ($this.val() == $this.attr('title'))
                $this.val('');
        });
        if ($type == 'password' && $.support.opacity) {
            $(this).prop('type', 'text');
            $(this).data('InputType', 'Password')
        } else if ($type == 'password' && !$.support.opacity) {
            $(this).addClass('IEPassword');
            $(this).attr({ value: '', title: '' });
        }
    });
    $(this).focus(function () {
        if (!$.support.opacity) {
            $(this).removeClass('IEPassword');
        }
        if ($(this).val() == $(this).attr('title')) {
            $(this).val('');
            if ($(this).data('InputType') == 'Password' && $.support.opacity) {
                $(this).prop('type', 'password');
            }
        }
    }).blur(function () {
        if ($(this).val() == '') {
            $(this).val($(this).attr('title'));
            if (!$.support.opacity) {
                $(this).addClass('IEPassword');
            }
            if ($.support.opacity) {
                $(this).prop('type', 'text');
            }
        }
    });
};
$(function () {
    $('.Hint[title]').inputHints();
});