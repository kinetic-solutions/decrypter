﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Decrypter
{
    public partial class Form1 : Form
    {
        private List<string> _files = new List<string>();
        private string _ConnectionString;
        private Dictionary<int, string> _OpenResourceQueries = new Dictionary<int, string>();
        private Dictionary<string, string> _badhtml;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            txtFRMinDate.Text = DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd");
            try
            {
                panelFixWebAgreements.Visible = Debugger.IsAttached || ConfigurationManager.AppSettings["AllowBulkUpdate"].ToLower() == "true";
                _ConnectionString = ConfigurationManager.ConnectionStrings["conn"].ToString();
            }
            catch { }
            if (string.IsNullOrEmpty(_ConnectionString) == false)
            {
                btnChooseConfig.Visible = false;
                label6.Visible = false;
                button1.Visible = false;
                cboDb.Visible = false;
                lblDb.Text = _ConnectionString;
                lblDb.Visible = true;
                LoadCombos();
            }
        }
        private void LoadCombos()
        {
            try
            {
                cboApp.Text = "";
                cboApp.Items.Clear();
                // cboResourceType.Items.Clear();
                cboSiteVer.Text = "";
                cboSiteVer.Items.Clear();
                if (!string.IsNullOrEmpty(_ConnectionString))
                {
                    cboApp.Items.Add("");
                    // cboResourceType.Items.Add("");
                    using (SqlConnection connection = new SqlConnection(_ConnectionString))
                    {
                        connection.Open();
                        using (SqlCommand command = new SqlCommand("SELECT DISTINCT ApplicationId FROM WebResources ORDER BY 1", connection))
                        {
                            try
                            {
                                SqlDataReader reader = command.ExecuteReader();
                                while (reader.Read())
                                {
                                    cboApp.Items.Add(reader[0].ToString());
                                }
                                reader.Close();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("WebResources query failed:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        //using (SqlCommand command = new SqlCommand("SELECT DISTINCT ResourceType FROM WebResources ORDER BY 1", connection))
                        //{
                        //    try
                        //    {
                        //        SqlDataReader reader = command.ExecuteReader();
                        //        while (reader.Read())
                        //        {
                        //            cboResourceType.Items.Add(reader[0].ToString());
                        //        }
                        //        reader.Close();
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        MessageBox.Show("WebResources query failed:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show("LoadCombos errored:" + ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Open(bool selected)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    string f = Path.GetTempFileName().Replace(".tmp", ".txt");
                    File.WriteAllText(f, GetWebResource(connection, selected));
                    _files.Add(f);
                    Process.Start(f);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Open errored:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //private void OpenAsPDF(bool selected)
        //{
        //    try
        //    {
        //        Cursor = Cursors.WaitCursor;
        //        using (SqlConnection connection = new SqlConnection(_ConnectionString))
        //        {
        //            connection.Open();
        //            string f = CreatePDF(GetWebResource(connection, selected));
        //            _files.Add(f);
        //            System.Diagnostics.Process.Start(f);
        //            connection.Close();
        //        }
        //    }
        //    catch (Exception ex2)
        //    {
        //        MessageBox.Show(ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    finally
        //    {
        //        Cursor = Cursors.Default;
        //    }
        //}
        //private string CreatePDF(string source)
        //{
        //    string f = Path.GetTempFileName().Replace(".tmp", ".pdf");
        //    byte[] bytes = GetTemplatePdfBytes(source);
        //    if (bytes != null)
        //    {
        //        try
        //        {
        //            int ArraySize = new int();
        //            ArraySize = bytes.GetUpperBound(0);
        //            FileStream fs1 = new FileStream(f, FileMode.OpenOrCreate, FileAccess.Write);
        //            fs1.Write(bytes, 0, ArraySize);
        //            fs1.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Error writing bytes:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //    return f;
        //}
        private void btnGo_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboIdent.Text) == false)
                Open(false);
        }
        //public static byte[] GetTemplatePdfBytes(string doc)
        //{
        //    byte[] result = null;

        //    try
        //    {
        //        doc = "<html>" + doc + "</html>";
        //        Winnovative.PdfConverter pdfConverter = new Winnovative.PdfConverter();

        //        pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
        //        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = Winnovative.PdfCompressionLevel.NoCompression;
        //        pdfConverter.PdfDocumentOptions.ShowHeader = false;
        //        pdfConverter.PdfDocumentOptions.ShowFooter = false;
        //        pdfConverter.PdfDocumentOptions.LeftMargin = 15;
        //        pdfConverter.PdfDocumentOptions.RightMargin = 15;
        //        pdfConverter.PdfDocumentOptions.TopMargin = 35;
        //        pdfConverter.PdfDocumentOptions.BottomMargin = 35;
        //        pdfConverter.PdfDocumentOptions.PdfPageSize = Winnovative.PdfPageSize.A4;
        //        pdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
        //        pdfConverter.PdfDocumentOptions.FitWidth = true;

        //        pdfConverter.PdfSecurityOptions.CanEditContent = false;
        //        pdfConverter.PdfSecurityOptions.CanEditAnnotations = false;
        //        pdfConverter.PdfSecurityOptions.CanAssembleDocument = false;
        //        pdfConverter.PdfSecurityOptions.OwnerPassword = "cranf13ld";
        //        pdfConverter.JavaScriptEnabled = true;

        //        //int conversionDelay = 2;

        //        //if (int.TryParse(KxDbConfiguration.GetConfiguration("PDFConversionDelaySeconds", "2"), out conversionDelay))
        //        //{
        //        //    pdfConverter.ConversionDelay = conversionDelay;
        //        //}


        //        if (!doc.ToLower().Contains("<html"))
        //        {
        //            doc = doc.Replace("\r", "<br/>");
        //            doc = doc.Replace("\n", "<br/>");
        //        }

        //        pdfConverter.LicenseKey = "Nbuouqu6oqmvuqurtKq6qau0q6i0o6Ojow==";

        //        //string tempfilepath = KxDbConfiguration.GetConfiguration("TempFileLocation");

        //        //string filename = Path.Combine(tempfilepath, emailtemplate + "_" + pdfno + ".pdf");


        //        try
        //        {
        //            result = pdfConverter.GetPdfBytesFromHtmlString(doc);
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //    catch (Exception ex2)
        //    {
        //        MessageBox.Show(ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return result;
        //}

        private string GetWebResource(SqlConnection connection, bool selected)
        {
            try
            {
                string wr = "", sql = "", sitever = "", identifier, app;
                app = cboApp.Text;
                if (selected)
                {
                    if (string.IsNullOrEmpty(app))
                    {
                        // Extract the app and sitever from the selected text
                        app = lbResults.SelectedItem.ToString();
                        int i = app.IndexOf(" (appid: ");
                        identifier = app.Substring(0, i);
                        app = app.Substring(i + 9);
                        i = app.IndexOf(", sitever: ");
                        sitever = app.Substring(i + 11);
                        app = app.Substring(0, i);
                        sitever = sitever.Substring(0, sitever.Length - 1);
                    }
                    else if (string.IsNullOrEmpty(cboSiteVer.Text))
                    {
                        // Extract sitever from the selected text
                        sitever = lbResults.SelectedItem.ToString();
                        int i = sitever.IndexOf(" (sitever: ");
                        identifier = sitever.Substring(0, i);
                        sitever = sitever.Substring(i + 11);
                        sitever = sitever.Substring(0, sitever.Length - 1);
                    }
                    else
                    {
                        sitever = cboSiteVer.Text;
                        identifier = lbResults.SelectedItem.ToString();
                    }
                    sql = string.Format("SELECT Data FROM WebResources WHERE applicationid = '{0}' AND siteversion = {1} AND identifier = '{2}'", app, sitever, identifier);
                }
                else if (cboDb.Text == "master")
                    sql = _OpenResourceQueries[cboIdent.SelectedIndex];
                else
                    sql = string.Format("SELECT Data FROM WebResources WHERE applicationid = '{0}' AND siteversion = {1} AND identifier = '{2}'", app, cboSiteVer.Text, cboIdent.Text);
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    try
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            wr = reader[0].ToString();
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("GetWebResource errored. Query failed. Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return "";
                    }
                }
                wr = DecoderEncoder.Base64Decode(wr);
                return wr;
            }
            catch (Exception ex2)
            {
                MessageBox.Show("GetWebResource errored: " + ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }
        private void cboApp_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                cboSiteVer.Items.Clear();
                cboSiteVer.Items.Add("");
                cboSiteVer.Text = "";
                cboIdent.Items.Clear();
                cboIdent.Items.Add("");
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SELECT DISTINCT SiteVersion FROM WebResources WHERE [ApplicationId] = '" + cboApp.Text + "' ORDER BY 1 DESC", connection))
                    {
                        try
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                cboSiteVer.Items.Add(reader[0].ToString());
                            }
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("WebResources query failed: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show("An error has occurred: " + ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboSiteVer_SelectedValueChanged(object sender, EventArgs e)
        {
            SiteVerChanged();
        }
        private void SiteVerChanged()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                cboIdent.Items.Clear();
                cboIdent.Items.Add("");
                if (string.IsNullOrEmpty(cboApp.Text) || string.IsNullOrEmpty(cboSiteVer.Text))
                    return;
                string sql;
                if (string.IsNullOrEmpty(cboResourceType.Text))
                    sql = string.Format("SELECT Identifier FROM WebResources WHERE [ApplicationId] = '{0}' AND SiteVersion = {1} ORDER BY 1", cboApp.Text, cboSiteVer.Text);
                else
                    sql = string.Format("SELECT Identifier FROM WebResources WHERE [ApplicationId] = '{0}' AND SiteVersion = {1} AND ResourceType = '{2}' ORDER BY 1", cboApp.Text, cboSiteVer.Text, cboResourceType.Text);

                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        try
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                cboIdent.Items.Add(reader[0].ToString());
                            }
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("WebResources query failed " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show("SiteVerChanged errored: " + ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                string suffix = "";
                cboIdent.Text = "";
                if (string.IsNullOrEmpty(_ConnectionString))
                {
                    MessageBox.Show("You have not chosen a web.config or database");
                    return;
                }
                lbResults.Items.Clear();
                string wr = "";
                string sql = "";
                int count = 0;
                if (!string.IsNullOrEmpty(cboApp.Text))
                    if (!string.IsNullOrEmpty(cboSiteVer.Text))
                        sql = string.Format("SELECT Data, Identifier FROM WebResources WHERE applicationid = '{0}' AND siteversion = {1}", cboApp.Text, cboSiteVer.Text);
                    else
                        sql = string.Format("SELECT Data, Identifier, siteversion FROM WebResources WHERE applicationid = '{0}' ", cboApp.Text);
                else if (!string.IsNullOrEmpty(cboSiteVer.Text))
                    sql = string.Format("SELECT Data, Identifier,applicationid FROM WebResources WHERE siteversion = {0}", cboSiteVer.Text);
                else
                    sql = "SELECT Data, Identifier,applicationid, siteversion FROM WebResources";
                sql += " ORDER BY Identifier";
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        try
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    suffix = "";
                                    wr = DecoderEncoder.Base64Decode(reader[0].ToString());
                                    if (string.IsNullOrEmpty(txtSearch.Text) || wr.ToLower().Contains(txtSearch.Text.ToLower()))
                                    {
                                        if (!string.IsNullOrEmpty(cboApp.Text))
                                        {
                                            if (string.IsNullOrEmpty(cboSiteVer.Text))
                                                suffix = " (sitever: " + reader[2].ToString() + ")";
                                        }
                                        else if (!string.IsNullOrEmpty(cboSiteVer.Text))
                                            suffix = " (appid: " + reader[2].ToString() + ")";
                                        else
                                            suffix = " (appid: " + reader[2].ToString() + ", sitever: " + reader[3].ToString() + ")";
                                        lbResults.Items.Add(reader[1].ToString() + suffix);
                                        count++;
                                        if (chkStopOnFirst.Checked)
                                            break;
                                    }
                                }
                                reader.Close();
                                if (lbResults.Items.Count == 0)
                                    MessageBox.Show("No matches found", "Search Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("GetWebResource: WebResources query failed " + ex.Message);
                        }
                    }
                    wr = DecoderEncoder.Base64Decode(wr);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        //private void btnPDF_Click(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(cboIdent.Text) == false)
        //        OpenAsPDF(false);
        //}

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (string f in _files)
            {
                try
                {
                    File.Delete(f);
                }
                catch { }
            }
        }

        private void btnOpenSelected_Click(object sender, EventArgs e)
        {
            if (lbResults.SelectedItem != null)
                Open(true);
        }

        //private void btnOpenSelectedAsPDF_Click(object sender, EventArgs e)
        //{
        //    if (lbResults.SelectedItem != null)
        //        OpenAsPDF(true);
        //}

        private void btnOpenAcceptance_Click(object sender, EventArgs e)
        {
            OpenAcceptance();
        }
        private void btnOpenAcceptanceAsPDF_Click(object sender, EventArgs e)
        {
            OpenAcceptance(true);
        }
        private void OpenAcceptance(bool aspdf = false)
        {
            try
            {
                string tbname, fieldname, where="";
                if (rbWAtb2.Checked)
                {
                    tbname = "WebStudentAllocation";
                    fieldname = "AcceptancePage";
                }
                else
                {
                    tbname = "ST2StudentCorrespondence";
                    fieldname = "Document";
                    where = " AND DocumentType = 'Web Agreement'";
                }
                if (string.IsNullOrEmpty(_ConnectionString))
                {
                    MessageBox.Show("You have not specified a web.config or database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                string ap = "";
                string sql = "";
                if (string.IsNullOrEmpty(txtAllocationId.Text) == false)
                    sql = string.Format("SELECT {0} FROM {1} WHERE AllocationId = {2}" + where, fieldname, tbname, txtAllocationId.Text);
                else if (rbWAtb1.Checked && string.IsNullOrEmpty(txtCorrespondenceId.Text)== false)
                    sql = string.Format("SELECT {0} FROM {1} WHERE CorrespondenceId = {2}" + where, fieldname, tbname, txtCorrespondenceId.Text);
                else
                {
                    MessageBox.Show("You have not specified an id", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        try
                        {
                            SqlDataReader reader = command.ExecuteReader();
                            if (reader.Read())
                            {
                                ap = Encoding.UTF8.GetString((byte[])reader[fieldname]);
                            }
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("OpenAcceptance errored. Query failed. Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                string f = "";
                if (chkOpenWAInNotepad.Checked)
                    f = Path.GetTempFileName().Replace(".tmp", ".txt");
                else
                    f = Path.GetTempFileName().Replace(".tmp", ".html");
                File.WriteAllText(f, ap);
                Process.Start(f);
            }
            catch (Exception ex)
            {
                MessageBox.Show("OpenAcceptance errored:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private string ExtractKeyValue(string s, string attr = "value")
        { 
            int i = s.IndexOf(attr);
            if (i > 0)
            {
                i = s.IndexOf("=", i + 1);
                if (i > 0)
                {
                    i = s.IndexOf(@"""", i + 1);
                    if (i > 0)
                    {
                        i++;
                        int j = s.IndexOf(@"""", i);
                        return s.Substring(i, j - i);
                    }
                }
            }
            return "";
        }
        private void btnChooseConfig_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "web.config|web.config";
                int sitever = 0;
                string appId = "";
                if (fd.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(fd.FileName))
                {
                    string[] lines = File.ReadAllLines(fd.FileName);
                    foreach (string line in lines)
                    {
                        if (line.Contains(@"<add name=""Web"" connectionString=") || line.Contains(@"<add name=""Kx"" connectionString="))
                        {
                            _ConnectionString = ExtractKeyValue(line, "connectionString");
                            lblconn.Text = _ConnectionString;
                        }
                        else if (line.Contains(@"add key=""SiteVersion"""))
                        {
                            if (int.TryParse(ExtractKeyValue(line, "SiteVersion"), out int sv))
                            {
                                sitever = sv;
                            }

                        }
                        else if (line.Contains(@"add key=""ApplicationId"""))
                        {
                            appId = ExtractKeyValue(line, "ApplicationId");
                        }
                    }
                }
                LoadCombos();
                if (!string.IsNullOrEmpty(appId))
                    cboApp.Text = appId;
                if (sitever > 0)
                {
                    cboSiteVer.Text = sitever.ToString();
                    SiteVerChanged();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                cboDb.Focus();
                button1.Enabled = false;
                string connectionString = "Data Source=ksl-dev-sql;Database=Master;Integrated Security=True;";
                cboDb.Items.Clear();
                cboDb.Items.Add("master");
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases ORDER BY name", con))
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                string dbname = dr[0].ToString();
                                if (dbname.ToLower().StartsWith("dev_") && dbname.ToLower().StartsWith("dev_kx") == false && dbname.ToLower().Contains("kxweb") && dbname.ToLower().EndsWith("old") == false)
                                    cboDb.Items.Add(dbname);
                            }
                        }
                    }
                }
                cboDb.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            { 
                Cursor = Cursors.Default; 
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void cboDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                _ConnectionString = "Data Source=ksl-dev-sql;Database=" + cboDb.Text + ";Integrated Security=True;";
                cboSiteVer.Visible = cboDb.Text != "master";
                lblSiteVer.Visible = cboDb.Text != "master";
                cboApp.Visible = cboDb.Text != "master";
                lblApp.Visible = cboDb.Text != "master";
                lblIndentifierContains.Visible = cboDb.Text == "master";
                txtIdentifierContains.Visible = cboDb.Text == "master";
                if (cboDb.Text != "master")
                    LoadCombos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occurred: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cboResourceType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cboDb.Text == "master")
            {
                if (string.IsNullOrEmpty(cboIdent.Text) == false)
                    LoadIdentifierAcrossAll();
            }
            else
                SiteVerChanged();
        }

        private void txtIdentifierContains_Validated(object sender, EventArgs e)
        {
            LoadIdentifierAcrossAll();
        }
        private void LoadIdentifierAcrossAll()
        {
            try
            {
                if (string.IsNullOrEmpty(txtIdentifierContains.Text))
                    return;
                List<string> dbs = new List<string>();
                int i = 0;
                string sql;
                Cursor = Cursors.WaitCursor;
                _OpenResourceQueries = new Dictionary<int, string>();
                if (string.IsNullOrEmpty(_ConnectionString))
                {
                    MessageBox.Show("You have not chosen a web.config or database");
                    return;
                }

                cboSiteVer.Items.Clear();
                cboSiteVer.Items.Add("");
                cboSiteVer.Text = "";
                cboIdent.Items.Clear();
                cboIdent.Items.Add("");

                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("SELECT name FROM sys.databases WHERE name LIKE 'Dev_%_KxWeb%'", connection))
                    {
                        try
                        {
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    dbs.Add(reader[0].ToString());
                                }
                                reader.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("sys.databases query failed: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    foreach (string db in dbs)
                    {
                        sql = "SELECT ApplicationId, SiteVersion, Identifier FROM [" + db + "].dbo.WebResources WHERE Identifier LIKE '%" + txtIdentifierContains.Text + "%'";
                        if (string.IsNullOrEmpty(cboResourceType.Text) == false)
                            sql += " AND ResourceType= '" + cboResourceType.Text + "'";
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            try
                            {
                                using (SqlDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        cboIdent.Items.Add(reader[2].ToString() + " (" + db + ")");
                                        _OpenResourceQueries.Add(i, string.Format("SELECT Data FROM [" + db + "].dbo.WebResources WHERE ApplicationId = '{0}' AND SiteVersion = {1} AND Identifier = '{2}'", reader[0].ToString(), reader[1].ToString(), reader[2].ToString()));
                                        i++;
                                    }
                                    reader.Close();
                                }
                            }
                            catch (Exception ex)
                            {
                                //MessageBox.Show("WebResources query failed: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex2)
            {
                MessageBox.Show("An error has occurred: " + ex2.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>We cannot have a DataReader open and then on same connection run an update query.
        /// So we get the update data first in batches of 100 and then do the update</remarks>
        private void btnFindReplace_Click(object sender, EventArgs e)
        {
            if (DoFindReplace(true))
                DoFindReplace(false);
        }
        private bool DoFindReplace(bool firstPass = false)
        {
            try
            {
                int count = 0, pkey, filelogcount = 0;
                int? nonmatchedpkey = null, matchedpkey = null;
                bool onerecord = false;
                string sql = "", tbname, fieldname, where = "", pkeyname, sqltop = "", debugfile = "";
                if (chkFRDebug.Checked)
                {
                    debugfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Debug.txt");
                    if (File.Exists(debugfile))
                        File.Delete(debugfile);
                }
                this.Cursor = Cursors.WaitCursor;
                if (rbWAtb2.Checked)
                {
                    tbname = "WebStudentAllocation";
                    fieldname = "WebStudentAllocation";
                    pkeyname = "AllocationId";
                    where = " CreationDate > '" + txtFRMinDate.Text + "' AND ";
                }
                else
                {
                    tbname = "ST2StudentCorrespondence";
                    fieldname = "Document";
                    where = " DocumentType = 'Web Agreement' AND CreationDate > '" + txtFRMinDate.Text + "' AND ";
                    pkeyname = "CorrespondenceID";
                }
                if (string.IsNullOrEmpty(_ConnectionString))
                {
                    MessageBox.Show("You have not specified a web.config or database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                if (string.IsNullOrEmpty(txtAllocationId.Text))
                {
                    MessageBox.Show("You have not entered an AllocationId" + Environment.NewLine + "You can enter '*' to process all records.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                else if (txtAllocationId.Text == "*")
                {
                    sql = string.Format("SELECT {0}, {1} FROM {2} WITH(NOLOCK) WHERE {3}{1} IS NOT NULL ORDER BY AllocationId DESC", pkeyname, fieldname, tbname, where);
                }
                else if (int.TryParse(txtAllocationId.Text, out int result))
                {
                    sql = string.Format("SELECT {0}, {1} FROM {2} WITH(NOLOCK) WHERE {4}AllocationId = {3}", pkeyname, fieldname, tbname, txtAllocationId.Text,where);
                    onerecord = true;
                }
                else if (txtAllocationId.Text.StartsWith(">"))
                    sql = string.Format("SELECT {0}, {1} FROM {2} WITH(NOLOCK) WHERE {4}AllocationId > {3} ORDER BY AllocationId DESC", pkeyname, fieldname, tbname, txtAllocationId.Text.Substring(1), where);
                string ap = "", apnew = "";
                using (SqlConnection connection = new SqlConnection(_ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        try
                        {
                            command.CommandTimeout = 300;
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    ap = Encoding.UTF8.GetString((byte[])reader[fieldname]);
                                    apnew = ap.Replace(txtWAFind.Text, Environment.NewLine + txtWAReplace.Text);
                                    if (ap.Contains(txtWAFind.Text))
                                    {
                                        count++;
                                        if (firstPass == false)
                                        {
                                            pkey = (int)reader[pkeyname];
                                            string updatesql = @"UPDATE " + tbname + " SET " + fieldname + " = @BinData WHERE " + pkeyname + " = " + pkey;
                                            if (chkFRDebug.Checked)
                                            {
                                                File.AppendAllText(debugfile, updatesql);
                                                File.AppendAllText(debugfile, Environment.NewLine + "BEFORE================================================");
                                                File.AppendAllText(debugfile, Environment.NewLine + ap);
                                                File.AppendAllText(debugfile, Environment.NewLine + "================================================"); 
                                                File.AppendAllText(debugfile, Environment.NewLine + "AFTER================================================");
                                                File.AppendAllText(debugfile, Environment.NewLine + apnew);
                                                File.AppendAllText(debugfile, Environment.NewLine + "================================================");
                                                filelogcount++;
                                                if (filelogcount == 10)
                                                    break;
                                            }
                                            else
                                            {
                                                using (SqlConnection updateconn = new SqlConnection(_ConnectionString))
                                                {
                                                    updateconn.Open();
                                                    using (SqlCommand updateCommand = new SqlCommand(updatesql, updateconn))
                                                    {
                                                        SqlParameter sqlParam = updateCommand.Parameters.AddWithValue("@BinData", Encoding.UTF8.GetBytes(apnew));
                                                        sqlParam.DbType = DbType.Binary;
                                                        try
                                                        {
                                                            updateCommand.ExecuteNonQuery();
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            MessageBox.Show("btnFindReplace_Click errored. Update query failed. Error : " + ex.Message + ",Primary Key: " + pkey, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                            return false;
                                                        }
                                                    }
                                                    updateconn.Close();
                                                }
                                            }
                                        }
                                        else if (matchedpkey == null)
                                        {
                                            matchedpkey = (int)reader[pkeyname];
                                        }
                                    }
                                    else if (firstPass && nonmatchedpkey == null)
                                    {
                                        nonmatchedpkey = (int)reader[pkeyname];
                                        if (chkStopOnFind.Checked)
                                            break;
                                    }
                                }
                                reader.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("btnFindReplace_Click errored. Query failed. Error : " + ex.Message + Environment.NewLine + "SQL: " + sql, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }
                if (firstPass)
                {
                    if (count == 0)
                    {
                        MessageBox.Show("There are no records to process", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    else
                    {
                        string msg = count + " records will be processed" + Environment.NewLine + Environment.NewLine + "First matched primary key is " + matchedpkey.Value;
                        if (nonmatchedpkey != null)
                            msg += Environment.NewLine + Environment.NewLine + "First non-matched primary key is " + nonmatchedpkey.Value;
                        if (chkFRDebug.Checked)
                            msg += Environment.NewLine + Environment.NewLine + "Do you want to debug the first 10 of these records?";
                        else
                            msg += Environment.NewLine + Environment.NewLine + "Do you want to update these records?";
                        return MessageBox.Show(msg, "Finished", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes;
                    }
                }
                else
                    MessageBox.Show("Done", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("btnFindReplace_Click errored:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }



        private void rbWAtb2_CheckedChanged(object sender, EventArgs e)
        {
            txtCorrespondenceId.Visible = rbWAtb1.Checked;
            lblCorrespondenceId.Visible = rbWAtb1.Checked;
        }

        private void rbWAtb1_CheckedChanged(object sender, EventArgs e)
        {
            txtCorrespondenceId.Visible = rbWAtb1.Checked;
            lblCorrespondenceId.Visible = rbWAtb1.Checked;
        }

        private void btnCheckLocationContent_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_ConnectionString))
            {
                MessageBox.Show("You have not chosen a web.config or database");
                return;
            }
            lbHTMLCheckResults.Items.Clear();
            _badhtml = new Dictionary<string, string>();
            using (SqlConnection connection = new SqlConnection(_ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT LocationContentID, Images, Facilities, MoreInfo FROM LocationContent", connection))
                {
                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = int.Parse(reader[0].ToString());
                                string images = DecoderEncoder.Base64Decode(reader[1].ToString());
                                string facilities = DecoderEncoder.Base64Decode(reader[2].ToString());
                                string moreinfo = DecoderEncoder.Base64Decode(reader[3].ToString());
                                ParseHTML(id, images, "Images");
                                ParseHTML(id, facilities, "Facilities");
                                ParseHTML(id, moreinfo, "More Info");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("btnCheckLocationContent_Click errored:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void ParseHTML(int id, string html, string typename)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            foreach(HtmlAgilityPack.HtmlParseError err in doc.ParseErrors)
            {
                lbHTMLCheckResults.Items.Add("id: " + id + ", type: " + typename + ", line no: " + err.Line + ", reason: " + err.Reason);
            }
            _badhtml.Add(id + "_" + typename, html);
        }
        private string GetSelectedBadHtml()
        {
            string[] arr = lbHTMLCheckResults.SelectedItem.ToString().Split(',');
            return _badhtml.SingleOrDefault(x => x.Key == arr[0].Substring(4) + "_" + arr[1].Substring(7)).Value;
        }
        private void btnViewBadHTML_Click(object sender, EventArgs e)
        {
           
            string f = Path.GetTempFileName().Replace(".tmp", ".txt");
            File.WriteAllText(f, GetSelectedBadHtml());
            _files.Add(f);
            Process.Start("Notepad++.exe", f);
        }

        private void btnEditBadHtml_Click(object sender, EventArgs e)
        {
            string[] arr = lbHTMLCheckResults.SelectedItem.ToString().Split(',');
            string typename = arr[1].Substring(7);
            dlgEdit f = new dlgEdit(int.Parse(arr[0].Substring(4)), typename, _ConnectionString, GetSelectedBadHtml());
            f.ShowDialog(this);
        }
    }
}
