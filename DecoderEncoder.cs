﻿using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksum;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System;

namespace Decrypter
{
    public static class DecoderEncoder
    {
        public static string Base64Decode(string source)
        {
            try
            {
                return Encoding.UTF8.GetString(Base64DecodeToByte(source));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Base64Decode errored: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }
        public static string Base64Encode(string source)
        {
            byte[] buffer;

            if (source != null && source.Length > 100)
            {
                buffer = CompressBytes(Encoding.UTF8.GetBytes(source));
            }
            else
            {
                buffer = Encoding.UTF8.GetBytes(source);
            }
            return "B64:" + Convert.ToBase64String(buffer);
        }
        private static byte[] Base64DecodeToByte(string source)
        {
            if (source.StartsWith("B64:"))
            {
                source = source.Remove(0, 4);
                try
                {
                    byte[] buffer = Convert.FromBase64String(source);
                    byte[] outbuffer = Decompress(buffer);

                    return outbuffer;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                UTF8Encoding utf = new UTF8Encoding();
                return utf.GetBytes(source);
            }
        }

        private static byte[] Decompress(byte[] Source)
        {
            ASCIIEncoding ae = new ASCIIEncoding();
            if (ae.GetString(Source).ToUpper().IndexOf("PK") == 0)
                return DeCompressBytes(Source);
            else
                return Source;
        }

        private static byte[] DeCompressBytes(byte[] bytesToDecompress)
        {
            byte[] writeData = new byte[4096];

            ZipInputStream s2 = new ZipInputStream(new MemoryStream(bytesToDecompress));

            ZipEntry entry = s2.GetNextEntry();

            if (entry != null) // Is this a valid zip??
            {
                MemoryStream outStream = new MemoryStream();

                while (true)
                {
                    int size = s2.Read(writeData, 0, writeData.Length);

                    if (size > 0)
                    {
                        outStream.Write(writeData, 0, size);
                    }
                    else
                    { break; }
                }

                s2.Close();
                byte[] outArr = outStream.ToArray();
                outStream.Close();
                return outArr;
            }
            else
                return bytesToDecompress;
        }

        private static byte[] CompressBytes(byte[] input)
        {
            using (MemoryStream buf = new MemoryStream())
            using (ZipOutputStream zip = new ZipOutputStream(buf))
            {
                Crc32 crc = new Crc32();
                zip.SetLevel(9);	// 0..9.

                ZipEntry entry = new ZipEntry("dummy");
                entry.DateTime = DateTime.Now;
                entry.Size = input.Length;

                crc.Reset();
                crc.Update(input);

                entry.Crc = crc.Value;

                zip.PutNextEntry(entry);
                zip.Write(input, 0, input.Length);
                zip.Finish();

                byte[] c = new byte[buf.Length];
                buf.Seek(0, SeekOrigin.Begin);
                buf.Read(c, 0, c.Length);

                zip.Close();

                return c;
            }
        }
    }
}
